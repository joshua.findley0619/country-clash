package lib;
import java.util.ArrayList;
/* The Goal of this project is to creating countries
 * with different components and then compare them against each
 * other based on different components.
 * Practice Reading Text Doc
 * Generating Records
 * Creating viewable tables
 *
 * @author Joshua Findley
 * @version 5/6/2023
 */


public class CountryTable {
	public Country France = new Country("France", 889, 69.3, 82.86);
	public Country US = new Country("United States", 2827, 69.2, 76.4);
	public Country UK = new Country("United Kingdom", 950, 69, 81.52);
	public Country Aust = new Country("Australia", 498, 66, 83.44 );
	public Country Can = new Country("Canada", 502, 67.5, 75.88);
	public Country Braz = new Country("Brazil", 129, 65.25, 75.88);
	public Country CHN = new Country("China", 608, 65.55, 76.91);
	public Country Ind = new Country("India", 28, 63, 69.73);
	public ArrayList<Country> myList = new ArrayList<>();
	
	public void setArray() {
		myList.add(France);
	}
	
	
	

}
