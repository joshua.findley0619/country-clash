#This sources are the top results of each bing search the AI listed
#Counties Total Medals According to worldpopulationreview.com
# Avg Height https://en.wikipedia.org/wiki/Average_human_height_by_country
# Avg Life exp https://www.worldometers.info/demographics/life-expectancy
#Country	Olympic medals	Average height	Life expectancy
Japan	555	165.3 cm	85.03 years
China	696	169.6 cm	77.47 years
USA	2980	170.1 cm	79.11 years
Australia	562	172.7 cm	82.05 years
Norway	528	173.4 cm	82.94 years
France	874	171.5 cm	83.13 years
Canada	525	171.7 cm	82.96 years
Germany	892	173.2 cm	81.88 years
Italy	742	168.1 cm	84.01 years
Spain	171	169.0 cm	83.99 years
Netherlands	451	177.1 cm	82.78 years
Brazil	150	169.0 cm	76.57 years
India	35	160.8 cm	70.42 years
Kenya	113	164.9 cm	67.47 years
South Korea	357	169.3 cm	83.50 years
Switzerland	358	171.5 cm	84.25 years
Sweden	661	173.6 cm	83.33 years
New Zealand	140	171.2 cm	82.80 years
Greece	121	172.5 cm	82.80 years